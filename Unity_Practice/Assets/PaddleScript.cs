﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleScript : MonoBehaviour {

	public float speed;

	public GameObject myBall;

	// Use this for initialization
	void Start () {
		ForPractice ();
	}
	
	// Update is called once per frame
	void Update () {
		MovePaddle ();

	}

	void MovePaddle(){
		if (Input.GetKey(KeyCode.DownArrow)) {
			transform.position += Vector3.down * speed *Time.deltaTime;
			//print ("Down");

		}else 
			if(Input.GetKey(KeyCode.UpArrow)) {
				transform.position += Vector3.up * speed *Time.deltaTime;
				//print("UpArrow");
				if(myBall!=null)
				myBall.GetComponent<BallScript> ().DestroyTheBall ();
			}

	}

	void ForPractice(){
		/*for (int i = 0; i < 10; i++) {
			print (i);
		}
		*/
		int i = 0;

		while (i < 10) {
			print (i);
			i++;
		}

	}





}
